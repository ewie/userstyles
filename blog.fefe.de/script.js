// ==UserScript==
// @name        blog.fefe.de
// @namespace   blog.fefe.de
// @version     1
// @grant       none
// ==/UserScript==

(function () {

  if (location.href.indexOf('https://blog.fefe.de') === 0) {

    function each(iterable, fn) {
      Array.prototype.forEach.call(iterable, fn);
    }

    function copy(iterable) {
      return Array.prototype.slice.call(iterable);
    }

    each(document.querySelectorAll('body > ul'), function (ul) {
      var dl = document.createElement('dl');
      ul.parentNode.insertBefore(dl, ul.nextSibling);
      each(ul.children, function (li) {
        if (li.tagName.toLowerCase() !== 'li') {
          return;
        }
        var a = li.firstChild;
        var m = a.href.match(/ts=([0-9a-f]+)/i);
        if (m) {
          // Resolve timestamp to local time and use as link text.
          // https://www.netaction.de/datenvisualisierung-von-fefes-blogzeiten/
          var ts = parseInt(m[1], 16) ^ 0xfefec0de;
          var d = new Date(ts * 1000);
          a.textContent = d.toLocaleTimeString('de-DE');

          var dt = document.createElement('dt');
          var dd = document.createElement('dd');

          dt.appendChild(a);
          dl.appendChild(dt);
          dl.appendChild(dd);

          // Move <li> child nodes to new <dd>. Copy child nodes instead iterating over the live collection.
          each(copy(li.childNodes), function (node) {
            dd.appendChild(node);
          });
        }
      });
      ul.remove();
    });

  }

}());
